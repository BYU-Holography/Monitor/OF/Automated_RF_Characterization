#include "ofApp.h"
#include <windows.h> // for sleep

// http://www.cplusplus.com/forum/beginner/14349/
void toClipboard( const std::string &s )
{
  OpenClipboard( 0 );
  EmptyClipboard();
  HGLOBAL hg=GlobalAlloc( GMEM_MOVEABLE, s.size() );
  if( !hg )
  {
    CloseClipboard();
    return;
  }
  memcpy( GlobalLock( hg ), s.c_str(), s.size() );
  GlobalUnlock( hg );
  SetClipboardData( CF_TEXT, hg );
  CloseClipboard();
  GlobalFree( hg );
}

void ofApp::open_GPIB_COM_port()
{
  string default_GPIB_COM_port = "COM9"; // change as needed
  int baud_rate = 9600;

  vector <ofSerialDeviceInfo> deviceList = GPIB_COM.getDeviceList();

  GPIB_COM_port_ID = -1;
  for( int i=0; i < deviceList.size(); i++ )
  {
    printf( "DeviceID %i, DeviceName %s DevicePath %s\n", deviceList[ i ].getDeviceID(), deviceList[ i ].getDeviceName().c_str(), deviceList[ i ].getDevicePath().c_str() );
    if( deviceList[ i ].getDeviceName() == "COM9" )
      GPIB_COM_port_ID = i;
  }

  if( GPIB_COM_port_ID != -1 )
  {
    int serial_setup_result = GPIB_COM.setup( GPIB_COM_port_ID, baud_rate );

    if( serial_setup_result )
    {
      printf( "connected to serial device %s\n", deviceList[ GPIB_COM_port_ID ].getDeviceName().c_str() );
    }
    else
      printf( "%s found but not available.\n", deviceList[ GPIB_COM_port_ID ].getDeviceName().c_str() );
  }
  else
    printf( "GPIB COM device not found.\n" );
}



//--------------------------------------------------------------
void ofApp::setup()
{
  ofSetWindowPosition( 400, 1000 );
  ofSetWindowShape( 600, 600 );

  open_GPIB_COM_port();

  // put series B into "max val" mode

  vector<string> command_list = { "++addr 18", "fa 0;fb 1000mz;hd;a1;b1;b2;stb?;done" };

  for( int i = 0; i < command_list.size(); i++ )
  {
    printf( "%s\n", command_list[ i ].c_str() );
    GPIB_COM.writeBytes( (unsigned char *) command_list[ i ].c_str(), strlen( command_list[ i ].c_str() ) );
    GPIB_COM.writeByte( '\n' );
    Sleep( 100 );
  }

  //append_carriage_return = true;
  //append_line_feed = true;

  //Sleep( 200 );

  //printf( "%i bytes received\n", GPIB_COM.available() );

  //printf( "\n" );

  ofSetBackgroundColor( 32, 32, 32 );
  ofSetColor( 32, 255, 32 );

  executing_command = COMMAND_NONE;
  attenuate_30dB = false;
}

//--------------------------------------------------------------
void ofApp::update()
{
  bool sweep_data_series = 0;

  if( executing_command != last_command )
  {
    printf( "\n" );

    last_command = executing_command;

    if( executing_command != COMMAND_NONE )
      printf( "Executing command " );

    switch( executing_command )
    {
    case COMMAND_START_SWEEP_A:     printf( "COMMAND_START_SWEEP_A" );     break;
    case COMMAND_START_SWEEP_B:     printf( "COMMAND_START_SWEEP_B" );     break;
    case COMMAND_SWEEP_GATHER_A:    printf( "COMMAND_SWEEP_GATHER_A" );    break;
    case COMMAND_SWEEP_GATHER_B:    printf( "COMMAND_SWEEP_GATHER_B" );    break;
    case COMMAND_SWEEP_REPORT_A:    printf( "COMMAND_SWEEP_REPORT_A" );    break;
    case COMMAND_SWEEP_REPORT_B:    printf( "COMMAND_SWEEP_REPORT_B" );    break;
    case COMMAND_CLIPBOARD_AB:      printf( "COMMAND_CLIPBOARD_AB" );      break;
    }
  }
  else
  {
    if( executing_command != COMMAND_NONE )
      printf( "." );
  }


  switch( executing_command )
  {
  case COMMAND_NONE:
    // print out whatever comes
    while( GPIB_COM.available() )
    {
      printf( "%c", (char) GPIB_COM.readByte() );
    }

    // if there's a command, start working on it
    if( command_list.size() )
    {
      executing_command = command_list.front();
      command_list.pop_front();
    }

    break;


  case COMMAND_START_SWEEP_A:
  case COMMAND_START_SWEEP_B:
  {
    string temp_string;

    sweep_data_series = executing_command == COMMAND_START_SWEEP_B;

    for( int i = 0; i < N_SWEEP_DATA; ++i )
      sweep_data[ sweep_data_series ][ i ] = -80.0f;

    received_data.clear();
    comma_counter = 0;

    GPIB_COM.flush();
    switch( executing_command )
    {
    case COMMAND_START_SWEEP_A: temp_string = "tra?\n"; break;
    case COMMAND_START_SWEEP_B: temp_string = "trb?\n"; break; // apparently "trb?b1;b2\n" doesn't return any data
    }

    GPIB_COM.writeBytes( (unsigned char *) temp_string.c_str(), temp_string.size() );

    //GPIB_COM.writeBytes(
    //  (unsigned char *)
    //  ( sweep_data_series ? "trb?\n" : "tra?\n" ),
    //  5
    //);

    command_timeout = false;
    command_last_active_time = ofGetElapsedTimeMillis();

    switch( executing_command )
    {
    case COMMAND_START_SWEEP_A: executing_command = COMMAND_SWEEP_GATHER_A; break;
    case COMMAND_START_SWEEP_B: executing_command = COMMAND_SWEEP_GATHER_B; break;
    default: executing_command = COMMAND_NONE; break;
    }
  }
  break;


  case COMMAND_SWEEP_GATHER_A:
  case COMMAND_SWEEP_GATHER_B:
    // save to array to format and copy to clipboard

    if( ofGetElapsedTimeMillis() - command_last_active_time > command_timeout_ms )
    {
      command_timeout = true;
      switch( executing_command )
      {
      case COMMAND_SWEEP_GATHER_A: executing_command = COMMAND_SWEEP_REPORT_A; break;
      case COMMAND_SWEEP_GATHER_B: executing_command = COMMAND_SWEEP_REPORT_B; break;
      default: executing_command = COMMAND_NONE; break;
      }
    }
    else
      while( GPIB_COM.available() )
      {
        command_last_active_time = ofGetElapsedTimeMillis();

        char c = GPIB_COM.readByte();

        // count and change delimiters
        if( c == ',' )
        {
          comma_counter++;
          received_data.push_back( '\r' ); // compatibility with Notepad
          received_data.push_back( '\n' );
        }
        else
          received_data.push_back( c );
      }

      // wait for anything else to come
      //Sleep( 100 );

      //if( comma_counter == 400 )
      //{
      //  printf( "RECEIVED SWEEP:\n%s", received_data.c_str() );
      //  executing_command = COMMAND_SWEEP_REPORT;
      //}

    break;


  case COMMAND_SWEEP_REPORT_A:
  case COMMAND_SWEEP_REPORT_B:

    sweep_data_series = executing_command == COMMAND_SWEEP_REPORT_B;

    if( comma_counter == ( N_SWEEP_DATA - 1 ) )
    {
      //printf( "RECEIVED SWEEP:\n%s", received_data.c_str() );

      toClipboard( received_data );

      // parse data
      string parse_value;
      int data_i = 0;

      for( int i = 0; i < received_data.size(); i++ )
      {
        if( received_data[ i ] == '\r' || received_data[ i ] == '\n' || i == ( received_data.size() - 1 ) )
        {
          // corner case
          if( i == ( received_data.size() - 1 ) )
            parse_value.push_back( received_data[ i ] );

          if( parse_value.size() )
          {
            sweep_data[ sweep_data_series ][ data_i ] =
              atof( parse_value.c_str() );

            data_i++;

            parse_value.clear();
          }
        }
        else
          parse_value.push_back( received_data[ i ] );
      }
    }

    // reset max val B series
    //if( sweep_data_series )
    //  GPIB_COM.writeBytes( ( unsigned char * ) "b1;b2\n", 6 );

    executing_command = COMMAND_NONE;
    break;


  case COMMAND_CLIPBOARD_AB:
  {
    // copy data of both sets into clipboard, formatted two-column style

    string temp_string;
    char temp_c_str[ 32 ];

    for( int i = 0; i < N_SWEEP_DATA; i++ )
    {
      sprintf( temp_c_str, "%.2f\t%.2f", sweep_data[ 0 ][ i ], sweep_data[ 1 ][ i ] );
      temp_string += temp_c_str;
      temp_string += "\r\n";
    }

    toClipboard( temp_string );

    executing_command = COMMAND_NONE;
  }
  break;

  default:
    break;
  }

  //toClipboard();
}

//--------------------------------------------------------------
void ofApp::draw()
{
  float x1, y1, x2, y2;

  // draw graphs
  for( int i = 1; i < N_SWEEP_DATA; i++ )
  {
    x1 = i - 1;
    y1 = -5 * ( sweep_data[ 0 ][ i - 1 ] - 30.0f );
    x2 = i;
    y2 = -5 * ( sweep_data[ 0 ][ i ] - 30.0f );

    ofSetColor( 192, 64, 32 );
    ofLine( ofPoint( x1, y1 ), ofPoint( x2, y2 ) );

    x1 = i - 1;
    y1 = -5 * ( sweep_data[ 1 ][ i - 1 ] - 30.0f );
    x2 = i;
    y2 = -5 * ( sweep_data[ 1 ][ i ] - 30.0f );

    ofSetColor( 32, 64, 192 );
    ofLine( ofPoint( x1, y1 ), ofPoint( x2, y2 ) );
  }
}

//--------------------------------------------------------------
void ofApp::keyPressed( int key )
{
  string temp_string;
  char temp_char;

  switch( key )
  {

  case 'a':
    //printf( "getting sweep A\n" );

    command_list.push_back( COMMAND_START_SWEEP_A );

    break;

  case 'b':
    //printf( "getting sweep B\n" );

    command_list.push_back( COMMAND_START_SWEEP_B );

    break;

  case 'c':
    //printf( "getting sweeps A & B\n" );

    command_list.push_back( COMMAND_START_SWEEP_A );
    command_list.push_back( COMMAND_START_SWEEP_B );
    command_list.push_back( COMMAND_CLIPBOARD_AB );

    break;

  case '1':
    GPIB_COM.writeBytes( ( unsigned char * ) "a1\n", 3 );
    break;

  case '2':
    GPIB_COM.writeBytes( ( unsigned char * ) "a2\n", 3 );
    break;

  case '3':
    GPIB_COM.writeBytes( ( unsigned char * ) "a3\n", 3 );
    break;

  case '4':
    GPIB_COM.writeBytes( ( unsigned char * ) "a4\n", 3 );
    break;

  case '5':
    GPIB_COM.writeBytes( ( unsigned char * ) "b1\n", 3 );
    break;

  case '6':
    GPIB_COM.writeBytes( ( unsigned char * ) "b2\n", 3 );
    break;

  case '7':
    GPIB_COM.writeBytes( ( unsigned char * ) "b3\n", 3 );
    break;

  case '8':
    GPIB_COM.writeBytes( ( unsigned char * ) "b4\n", 3 );
    break;

  case '0':
    GPIB_COM.writeBytes( ( unsigned char * ) "stb?\n", 5 );
    break;

  case 'l':
    GPIB_COM.writeBytes( ( unsigned char * ) "++loc\n", 6 );
    break;

  case 'i':
    GPIB_COM.writeBytes( ( unsigned char * ) "id?\n", 4 );
    break;

  case 'q':
    // toggle amplitude scale
    attenuate_30dB = !attenuate_30dB;

    if( attenuate_30dB )
    {
      printf( "changing scale to +30dBm - -50dBm - USE 30 dB ATTENUATOR\n" );
      temp_string = "roffset 30dm;roffset?;dl 0db;hd\n";
    }
    else
    {
      printf( "changing scale to 0dBm - -80dBm - DON'T USE ATTENUATOR\n" );
      temp_string = "roffset 0dm;roffset?;dl 0db;hd\n";
    }

    GPIB_COM.writeBytes( (unsigned char *) temp_string.c_str(), temp_string.size() );
    break;

  }
}

//--------------------------------------------------------------
void ofApp::keyReleased( int key )
{

}

//--------------------------------------------------------------
void ofApp::mouseMoved( int x, int y )
{

}

//--------------------------------------------------------------
void ofApp::mouseDragged( int x, int y, int button )
{

}

//--------------------------------------------------------------
void ofApp::mousePressed( int x, int y, int button )
{

}

//--------------------------------------------------------------
void ofApp::mouseReleased( int x, int y, int button )
{

}

//--------------------------------------------------------------
void ofApp::mouseEntered( int x, int y )
{

}

//--------------------------------------------------------------
void ofApp::mouseExited( int x, int y )
{

}

//--------------------------------------------------------------
void ofApp::windowResized( int w, int h )
{

}

//--------------------------------------------------------------
void ofApp::gotMessage( ofMessage msg )
{

}

//--------------------------------------------------------------
void ofApp::dragEvent( ofDragInfo dragInfo )
{

}
